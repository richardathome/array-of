# ArrayOf

## A PHP 8.1 Typesafe array

Example usage:

use Richbuilds/ArrayOf;

```php
$array = new ArrayOf(User::class);

$array[] = new User(); // works

$array[] = new stdObj(); // fails with type error
```