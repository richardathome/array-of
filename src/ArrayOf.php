<?php
declare(strict_types=1);

namespace Richbuilds\Generics;

use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;
use Throwable;
use Traversable;
use TypeError;

/**
 * Implements a type safe array
 */
class ArrayOf implements IteratorAggregate, ArrayAccess
{
    private array $values = [];


    /**
     * Set up the type safe array
     *
     * @param string $class
     *
     * @param array $values
     * @throws Throwable
     */
    public function __construct(
        private readonly string $class,
        array                   $values = [])
    {
        $this->set($values);
    }


    /**
     * Sets an element or elements of the array
     *
     * @param array|string|int $key
     *
     * @param mixed|null $value
     *
     * @return $this
     *
     * @throws Throwable
     */
    public function set(array|string|int $key, mixed $value = null): self
    {
        $original_values = $this->values;

        try {
            if (is_array($key)) {

                foreach ($key as $k => $v) {
                    $this->offsetSet($k, $v);
                }

            } else {
                $this->offsetSet($key, $value);
            }
        }
        catch (Throwable $e) {
            $this->values = $original_values;
            throw $e;
        }

        return $this;
    }


    /**
     * Allows foreach($array as ...)
     *
     * @return Traversable
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->values);
    }


    /**
     * Allows isset($array[$offset])
     *
     * @param $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return isset($this->values[$offset]);
    }


    /**
     * Allows $array[$offset] access
     *
     * @param $offset
     * @return mixed
     */
    public function offsetGet($offset): mixed
    {
        return $this->values[$offset];
    }


    /**
     * Allows $array[$offset] = foo
     *
     * @param $offset
     * @param $value
     *
     * @throws TypeError
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (!$value instanceof $this->class) {
            throw new TypeError('invalid type - expected ' . $this->class . ' got ' . get_debug_type($value));
        }

        if ($offset === null) {
            $this->values[] = $value;
        } else {
            $this->values[$offset]= $value;
        }
    }


    /**
     * Allows unset($array[$offset]);
     *
     * @param $offset
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->values[$offset]);
    }
}